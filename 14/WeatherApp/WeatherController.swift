
import UIKit
import SwiftyJSON
import RealmSwift

class WeatherController: UIViewController {
    
    @IBOutlet weak var currentWeather: UILabel!
    @IBOutlet weak var feelsLikeWeather: UILabel!
    @IBOutlet weak var nameTown: UILabel!
    @IBOutlet weak var minT: UILabel!
    @IBOutlet weak var maxT: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        1) чтение данных из базы данных и отобразить на экране
//        2) вызов методов получения новых данных
//        3) сохраняется в БД и выводится на экран
        let weatherLoader = WeatherLoader()
        weatherLoader.delegate = self
        weatherLoader.loadFirst()
        weatherLoader.loadWeather()
        weatherLoader.loadWeatherFromCache()
    }
}

extension WeatherController: LoadWeatherDelegate {
    func loaded (
        currentTemperature: String,
        feelsLikeTemperature: String,
        minTemperature: String,
        maxTemperature: String,
        nameTownLabel: String
    ) {
        currentWeather.text = currentTemperature
        feelsLikeWeather.text = feelsLikeTemperature
        minT.text = minTemperature
        maxT.text = maxTemperature
        nameTown.text = nameTownLabel
    }
}
